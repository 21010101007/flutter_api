import 'dart:convert';
import 'package:apiscreen/insertdata.dart';
import 'package:apiscreen/page3.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Page2 extends StatefulWidget {
  const Page2({Key? key}) : super(key: key);

  @override
  State<Page2> createState() => _Page2State();
}

class _Page2State extends State<Page2> {
  List<Map> Users = [];

  @override
  void initState() {
    Map<String, dynamic> map = {};
    map["Name1"] = 'Piizza';
    map["Image1"] = 'assets/images/download.jpg';

    Users.add(map);

    map = {};
    map["Name1"] = 'Burger';
    map["Image1"] = 'assets/images/download (1).jpg';
    Users.add(map);

    map = {};
    map["Name1"] = 'Vegan';
    map["Image1"] = 'assets/images/images.jpg';
    Users.add(map);

    map = {};
    map["Name1"] = 'Desert';
    map["Image1"] = 'assets/images/images (1).jpg';
    Users.add(map);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("APi Demo"),
        actions: [
          InkWell(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                return InsertData(map: null);
              },),).then((value) {
                setState(() {

                });
              },);
            },
            child: Icon(Icons.add),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Container(
              color: Colors.blueAccent,
              height: 270,
            ),
            Container(
              margin: EdgeInsets.only(top: 100, left: 20),
              child: Text(
                "Discover places \nand Restaurants",
                style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(35.0),
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 205),
                    child: TextField(
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          border: OutlineInputBorder(),
                          hintText: 'Type location and restaurants',
                          suffixIcon: Icon(Icons.arrow_forward_outlined)),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 330, left: 15),
              child: Text(
                "Near you",
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 359, left: 15),
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 15),
                    height: 210,
                    child: FutureBuilder<http.Response>(
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          dynamic jsonData =
                              jsonDecode(snapshot.data!.body.toString());
                          return ListView.builder(
                            scrollDirection: Axis.horizontal,
                            physics: BouncingScrollPhysics(),
                            itemCount: jsonData.length,
                            itemBuilder: (context, index) {
                              return InkWell(
                                onTap: () {
                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (context) {
                                        return Page3(
                                          name: jsonData[index]["name"],
                                          image: jsonData[index]["avatar"],
                                          time: jsonData[index]["time"],
                                          add: jsonData[index]["subtitle"],
                                        );
                                      },
                                    ),
                                  );
                                },
                                child: Container(
                                  width: 180,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(10),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Color.fromRGBO(0, 0, 0, 0.4),
                                            blurRadius: 5,
                                            offset: Offset(2, 2))
                                      ]),
                                  margin: EdgeInsets.only(right: 25),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Stack(
                                        children: [
                                          Container(
                                            width: double.infinity,
                                            height: 130,
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              child: Image.network(
                                                jsonData[index]["avatar"],
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                          Positioned(
                                            top: 10,
                                            right: 10,
                                            child: Container(
                                              width: 30,
                                              height: 30,
                                              decoration: BoxDecoration(
                                                color: Colors.grey,
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                              ),
                                              child: InkWell(
                                                onTap: () {
                                                  deleteUser(
                                                          jsonData[index]["id"])
                                                      .then(
                                                    (value) {
                                                      setState(() {});
                                                    },
                                                  );
                                                },
                                                child: Icon(
                                                  Icons.delete,
                                                  color: Colors.red,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 10, right: 10),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Container(
                                                  width: 120,
                                                  child: Text(
                                                    jsonData[index]["name"],
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                        fontSize: 20,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                ),
                                                InkWell(
                                                  onTap: () {
                                                    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                                                      return InsertData(map: jsonData[index],);
                                                    },),).then((value) {
                                                      setState(() {

                                                      });
                                                    },);
                                                  },
                                                  child: Icon(
                                                    Icons.edit,
                                                    color: Colors.grey,
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Text(
                                              jsonData[index]["subtitle"],
                                              style: TextStyle(fontSize: 15),
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                          );
                        } else {
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        }
                      },
                      future: getData(),
                    ),
                  ),
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 600, left: 15, bottom: 15),
                  child: Text(
                    "Categories",
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  height: 100,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: Users.length,
                    itemBuilder: (context, index) {
                      return Container(
                        width: 70,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          // boxShadow: [
                          // BoxShadow(
                          //     color: Color.fromRGBO(0, 0, 0, 0.4),
                          //     blurRadius: 5,
                          //     offset: Offset(2, 2))
                          // ],
                        ),
                        margin: EdgeInsets.only(right: 15, left: 15),
                        child: Column(
                          children: [
                            Container(
                              width: double.infinity,
                              height: 70,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(10),
                                child: Image.asset(
                                  Users[index]['Image1'],
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            // SizedBox(
                            //   height: 5,
                            // ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 10, right: 10),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(top: 5, left: 5),
                                    child: Text(
                                      Users[index]['Name1'],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Future<http.Response> getData() async {
    http.Response res = await http
        .get(Uri.parse('https://630ee8b537925634188341af.mockapi.io/items'));
    print(res.body);
    return res;
  }

  Future<void> deleteUser(id) async {
    http.Response res = await http.delete(
        Uri.parse("https://630ee8b537925634188341af.mockapi.io/items/$id"));
  }
}
